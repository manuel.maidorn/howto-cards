---
layout: default
title: Home
order: 1
---

# LCSB How-to Cards

The Bioinformatics Core and Lab Support teams assist researchers from the Luxembourg Centre for Systems Biomedicine ([LCSB](https://www.uni.lu/lcsb)) with the organization, management, and curation of research data through its R3 initiative.
The How-to cards are intended to provide practical guidance in implementing Data Management, Data Protection, IT setup, lab support, and others.

## All categories

<!-- index -->

### Access

* [LUMS account](./external/access/lums-passwords/)
* [Managing your passwords](./external/access/passwords/)
* [VPN/CERBERE connection](./external/access/vpn-cerbere-access/)

### Contribute

* [Git clients](./external/contribute/git-clients/)
* [How to add or edit a howto card](./external/contribute/add-edit-card/)
* [Markdown](./external/contribute/markdown/)

### Daisy

* [DAISY User Guide](./external/daisy/)

### Exchange channels

* [AsperaWEB Quick Guide](./external/exchange-channels/duma/)
* [Owncloud](./external/exchange-channels/owncloud/)
* [Sharing calendar in Microsoft Exchange](./external/exchange-channels/calendar/)

### Integrity

* [Encrypting Files and Folders](./external/integrity/encryption/file/)
* [Encrypting the Startup Disk for Your Laptop/Desktop](./external/integrity/encryption/disk/)
* [Ensuring Integrity of Data Files with Checksums](./external/integrity/checksum/)
* [Naming files](./external/integrity/naming/)
