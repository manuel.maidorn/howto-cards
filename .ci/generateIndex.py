
import os, re
from os import path

def line_prepender(filename, line):
    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)

def remove_header(localroot, root, filename, n=5):
    nfirstlines = []

    #cwd = os.getcwd()

    os.chdir(localroot)
    #print("Local root folder:" + os.getcwd())

    os.chdir(root)
    #print("Local folder:" + os.getcwd())

    # count the number of lines
    count = 0
    headerCheck = False
    with open(filename, 'r') as f:
        for line in f:
            count += 1

            # check if the header is actually a header
            if (count == 0 or count == n) and line[0:3] == "---":
                headerCheck = True

    # remove the header
    if count > n and headerCheck:
        with open(filename) as f, open("tmp"+filename, "w") as out:
            for _ in range(n):
                nfirstlines.append(next(f))
            for line in f:
                out.write(line)

        os.remove(filename)
        os.rename("tmp"+filename, filename)
        print(" - Old header removed.")

    # change back to the local root
    os.chdir(localroot)
    #print("exit folder:" + os.getcwd())

# loop through the entire internal tree
localroot = os.getcwd()


# generate the index properly speaking
cardDirs = ["internal", "external"]
sections = []

# determine first the directories
for direct in cardDirs:
    if path.isdir(direct):
        dirs = os.listdir(direct)

        for d in dirs:
            if d[0] != ".":
                sections.append(d)

sections = list(set(sections))
sections.sort()

index = ""
localIndexArr = [[]] * len(sections)
for folder in cardDirs:

    # check if folder exists
    if path.isdir(folder):
        dirs = os.listdir(folder)
        dirs = sorted(dirs)

        for d in dirs:
            if d[0] != ".":
                # set the header of the section
                #index += "\n### " + d.replace("-", " ").capitalize() + "\n"


                # get the index of the section
                indexS = sections.index(d)

                #print(indexS)
                if len(localIndexArr[indexS]) == 0:
                    localIndexArr[indexS] = ["\n"]

                # walk through the folders with all the cards
                for root, dirs, files in os.walk(folder+"/"+d):
                    for file in files:
                        if file.endswith(".md"):
                            fileName = os.path.join(root, file)

                            # ignore subsections (.md files that start with _)
                            if file[0] != "_":
                                print(" > Generating header for: " + fileName)

                                # remove the previous header
                                remove_header(localroot, root, file, 8)

                                # generate a permalink
                                permalink = "/" + root + "/"

                                # generate the shortcut
                                shortcut = re.sub(folder, '', root)

                                # remove the first /
                                shortcut = shortcut[1:]

                                # replace the / with a :
                                shortcut = re.sub('/', ':', shortcut)

                                # define the header for each card
                                header = "---\n"
                                header += "layout: page\n"
                                header += "permalink: " + permalink + "\n"
                                header += "shortcut: " + shortcut + "\n"
                                header += "redirect_from:\n"
                                header += "  - /cards/" + shortcut + "\n"
                                header += "  - /" + folder + "/cards/" + shortcut + "\n"
                                header += "---"

                                # add the header properly speaking
                                line_prepender(fileName, header)

                                # open file and get the title after the header
                                count = 0
                                title = ""
                                bp = 9
                                with open(fileName, 'r') as f:
                                    for line in f:
                                        count += 1
                                        if count == bp:
                                            if len(line) > 2:
                                                title = line
                                                break
                                            else:
                                                bp += 1

                                # remove first and last chars
                                title = title.rstrip("\n\r")
                                title = title[2:]

                                #index += "  * [" + title + "](./" + root + "/" + "\n"
                                localIndexArr[indexS].append("* [" + title + "](./" + root + "/" + ")\n")

                                # output
                                print(" + New header added.")
                                print("-----------------------")

                # join all subcategories to the index
                localIndexArr[indexS].sort()

print(localIndexArr)

# determine the index
k = 0
for s in sections:
    index += "\n### " + s.replace("-", " ").capitalize() + "\n"
    index += ''.join(localIndexArr[k])
    k += 1

# output the index
#print(index)
# Read in the file
indexFile = "index.md"
filedata = ""
with open(indexFile, 'r') as file :
    for line in file:
        filedata += line

        # stop reading once the index place holder has been reached
        if re.search("<!-- index -->", line):
            filedata += "[[ index ]]"
            break

# Replace the target string
filedata = filedata.replace('[[ index ]]', index)

# Write the file out again
with open(indexFile, 'w') as file:
  file.write(filedata)

print("\n > New index generated and saved in " + indexFile)