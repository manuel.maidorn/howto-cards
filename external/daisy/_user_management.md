
# 7 Login, Types of Users and Permissions


Upon successful installation of DAISY, going to the web address ```https://${IP_ADDRESS_OR_NAME_OF_DEPLOYMENT_SERVER}```
should display the login page.

 ![Alt](img/login.png)
 <center>DAISY Login Page</center>

Based on the authentication configuration made for your deployment, you may login by:

* user definitions in an existing LDAP directory, e.g. institutional/uni credentials
* user definitions maintained within the DAISY database.


<mark>DAISY is intended to be used mostly by three categories of end users in a Biomedical research institution</mark>; primarily  Research staff e.g. principle investigators, lab members, legal support team, and IT and data management specialists.

Specifically, DAISY has the following **user groups**  to support record access control;

* **standard**: This is the default role assigned to all users. All DAISY users can view all Dataset, Project, Contract and Definitions (Cohorts, Partner, Contact). The document attachments of records are excluded from this view permission.
* **vip**: This role is typically given to research principle investigators. VIP users have all privileges on the records they own, meaning the records where the user has been appointed as the ``Local Custodian``. They also have the right to give permissions to others on these records.
* **legal**: This role is given to users that will be managing _Contract_ records. Legal personnel will be able to create view and edit contract as well as view all other records in DAISY and manage their document attachments
* **auditor**: This role would designed to an external person, who is given view-only temporary access to all DAISY records. This would typically happening an audit scenario.



DAISY supports fine-grained permission management with the following categories of permissible actions. The ability to _View_ records, _View Document_ attachments of records, ability to _Edit_ and _Delete_ records. The ability to _Administer Permissions_.



| User Category | Administer Permissions | Delete | Edit | View | View  Document Attachments |
| -------------|:-------------:|:-------------:|:-------------:|:-------------:|:-----|
| superuser | P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub> | P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub>| P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub>| P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub> | P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub>|
| standard |  | | | P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub> | |
| vip | P<sub>own</sub>, D<sub>own</sub> | P<sub>own</sub>, D<sub>own</sub>| P<sub>own</sub>, D<sub>own</sub>| P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub>  | P<sub>own</sub>, D<sub>own</sub>, C<sub>own</sub> |
| auditor |  | | |P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub>| P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub> |
| legal | C<sub>all</sub> | C<sub>all</sub> | P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub> | P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub> | P<sub>all</sub>, D<sub>all</sub>, C<sub>all</sub>, Def<sub>all</sub> |



<br />
<br />
<br />
<div style="text-align: right;"> <a href="#top">Back to top</a> </div>
<br />
<br />
<br />