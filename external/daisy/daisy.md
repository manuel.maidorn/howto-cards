---
layout: page
permalink: /external/daisy/
shortcut: daisy
redirect_from:
  - /cards/daisy
  - /external/cards/daisy
---
# DAISY User Guide

{:.no_toc}
This howto-card is the user guide for the Data Information System (DAISY). If it is your first time with the guide then start with Section **DAISY at a Glance**.

* TOC
{:toc}

{% include_relative _at_a_glance.md %}
{% include_relative _interface_conventions.md %}
{% include_relative _project_management.md %}
{% include_relative _dataset_management.md %}
{% include_relative _contract_management.md %}
{% include_relative _definitions_management.md %}
{% include_relative _user_management.md %}
